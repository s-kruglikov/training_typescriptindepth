This project is a seed project for TypeScript forked from [https://github.com/VZhyrytskiy/TS-In-Depth-WebpackEnv]
It uses webpack and webpack-dev-server.

Run 
> npm install

Commands
    > "start": "webpack-dev-server --open",
    > "build": "webpack --config webpack.config.js",
    > "watch": "webpack --watch",