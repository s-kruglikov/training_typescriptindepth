
import { Category } from './enums';
import { UniversityLibrarian, ReferenceItem } from './classes';
import { Book, Logger, Author, Librarian, Magazine } from './interfaces';
import * as util from './lib/utility-functions';
import Encyclopedia from './encyclopedia';
import RefBook from './encyclopedia';
import Shelf from './shelf';

util.showHello('greeting', 'TypeScript');

// Task1();
// Task2();
// Task3();
// Task4();
// Task5();
// Task6();
// Task7();
// Task8();
// Task9();
// Task10();
// Task11();
// Task12();
// Task16();
// Task17();
// Task18();
// Task19();
// Task21();
// Task22();
// Task23();
// Task24();

// #region Task 1 - Basic Types

function Task1() {
	util.logFirstAvailable(util.getAllBooks());
}

// #endregion

// #region Task 2 - Enums

function Task2() {
	const javascriptBooksTitles: string[] = util.getBookByCategory(Category.JavaScript);
	util.logBookTitles(javascriptBooksTitles);
}

// #endregion

// #region Task 3 - Arrow functions

function Task3() {
	util.getAllBooks().forEach(element => {
		if (element.category === Category.JavaScript)
			console.log(element.title);
	});

	console.log(util.getBookByID(2).title);
}

// #endregion

// #region Task 4 - Function type

function Task4() {
	let myId: string = util.createCustomerId('Ann', 10);
	let idGenerator: (name: string, id: number) => string;

	idGenerator = (name: string, id: number) => {
		return `${name}${id}`;
	};

	idGenerator = util.createCustomerId;

	console.log(idGenerator('Ann', 10));
}

// #endregion

// #region Task 5 - Optional, Default and Rest Parameters

function Task5() {
	util.createCustomer('Tony');
	util.createCustomer('Tony', 30);
	util.createCustomer('Tony', 30, 'Miami');


	let myBooks = util.checkoutBooks('Tony', 1, 2, 4);

	myBooks.forEach(element => {
		console.log(element);
	});
}

// #endregion

// #region Task 6 - Function Overloading

function Task6() {
	util.logBookTitles(util.getBookTitles('Liang Yuxian Eugene'));
	util.logBookTitles(util.getBookTitles(true));
}

// #endregion

// #region Task 7 - Defining interface

function Task7() {
	const myBook: Book = {
		id: 5,
		title: 'Colors, Backgrounds, and Gradients',
		author: 'Eric A. Meyer',
		available: true,
		category: Category.CSS,
		year: 2015,
		copies: 3,
		markDamaged: (reason: string) => { console.log(`Reason: ${reason}`); }
	};

	util.printBook(myBook);
	myBook.markDamaged('Missing back cover');
}

// #endregion

// #region Task 8 - Interface for Function Types

function Task8() {
	let logDamage: Logger;
	logDamage = (reason: string) => {
		console.log(`Reason: ${reason}`);
	};
	logDamage('Missing back cover');
}

// #endregion

// #region Task 9 - Extending Interface

function Task9() {
	let favoriteAuthor: Author = {
		name: 'Tony',
		email: 'test@email.com',
		numBooksPublished: 2
	};

	console.log(`Favorite author is ${favoriteAuthor.name}`);

	let favoriteLibrarian: Librarian = {
		name: 'Ann',
		email: 'ann@email.com',
		department: 'sales',
		assistCustomer: (custName: string) => {
			console.log(`Assisting customer ${custName}`);
		}
	};

	console.log(`Favorite librarian is ${favoriteLibrarian.name}`);
}

// #endregion

// #region Task 10 - Interfaces for Class Types

function Task10() {
	const favoriteLibrarian = new UniversityLibrarian();
	favoriteLibrarian.name = 'Ann';
	favoriteLibrarian.assistCustomer('Boris');
	favoriteLibrarian.assistFaculty = () => console.log('test');
	// favoriteLibrarian.teachCommunity = () => console.log('test');

	favoriteLibrarian.assistFaculty();
	favoriteLibrarian.teachCommunity();
}

// #endregion

// #region Task 11 - Creating and Using Classes

function Task11() {
	// const ref = new ReferenceItem('Title', 2004);
	// ref.printItem();
	// ref.publisher = 'Random publisher';
	// console.log(ref.publisher);
}

// #endregion

// #region Task 12 - Extending Classes

function Task12 () {
	const refBook: ReferenceItem = new Encyclopedia('MyTitle', 2005, 1);
	refBook.printItem();
	refBook.printCitation();
}

// #endregion

// #region Task 13 - Creating Abstract Classes

// ReferenceItem marked as abstract.
// printCitation() added to encyclopedia.

// #endregion

// #region Task 14 - Using Namespaces

// Please find in NamespaceDemo folder.

// #endregion

// #region Task 15 - Export and Import

// imports and exports are appplied through out the project.

// #endregion

// #region Task 16 - Default Export

function Task16 () {
	const refBook: ReferenceItem = new RefBook('MyTitle', 2005, 1);
	refBook.printItem();
	refBook.printCitation();
}

// #endregion

// #region Task 17 - Generic Functions

function Task17() {
	const inventory: Array<any> = [
		{ id: 10, title: 'The C Programming Language', author: 'K & R', available: true, category: Category.Software },
		{ id: 11, title: 'Code Complete', author: 'Steve McConnell', available: true, category: Category.Software },
		{ id: 12, title: '8-Bit Graphics with Cobol', author: 'A. B.', available: true, category: Category.Software },
		{ id: 13, title: 'Cool autoexec.bat Scripts!', author: 'C. D.', available: true, category: Category.Software }
	];
	const numbers: number[] = [1, 2, 3, 4, 5, 6];

	const reducedInventory = util.purge(inventory);
	const reducedNumbers = util.purge(numbers);

	for (const item of reducedInventory) {
		console.log(item);
	}

	for (const item of reducedNumbers) {
		console.log(item);
	}
}

// #endregion

// #region Task 18 - Generic Interfaces and Classes

function Task18 () {
	const books: Array<Book> = [
		{ id: 10, title: 'The C Programming Language', author: 'K & R', available: true, category: Category.Software },
		{ id: 11, title: 'Code Complete', author: 'Steve McConnell', available: true, category: Category.Software },
		{ id: 12, title: '8-Bit Graphics with Cobol', author: 'A. B.', available: true, category: Category.Software },
		{ id: 13, title: 'Cool autoexec.bat Scripts!', author: 'C. D.', available: true, category: Category.Software }
	];

	const bookShelf = new Shelf<Book>();
	for (const book of books) {
		bookShelf.add(book);
	}

	console.log(bookShelf.getFirst());

	const magazines: Array<Magazine> = [
		{ title: 'Programming Language Monthly', publisher: 'Code Mags' },
		{ title: 'Literary Fiction Quarterly', publisher: 'College Press' },
		{ title: 'Five Points', publisher: 'GSU' }
	];

	const magazinesShelf = new Shelf<Magazine>();

	for (const magazine of magazines) {
		magazinesShelf.add(magazine);
	}

	console.log(magazinesShelf.getFirst());
}

// #endregion

// #region Task19 - Generic Constraints

function Task19() {
	const magazines: Array<Magazine> = [
		{ title: 'Programming Language Monthly', publisher: 'Code Mags' },
		{ title: 'Literary Fiction Quarterly', publisher: 'College Press' },
		{ title: 'Five Points', publisher: 'GSU' }
	];

	const magazinesShelf = new Shelf<Magazine>();

	for (const magazine of magazines) {
		magazinesShelf.add(magazine);
	}

	magazinesShelf.printTitles();

	// console.log(magazinesShelf.find('Code Complete'));
	console.log(magazinesShelf.find('Five Points'));
}

// #endregion

// #region Task 20.1, 20.2 - Decorators

// find in decorators.ts

// #endregion

// #region Task 21 - Method Decorator

function Task21 () {
	const favoriteLibrarian = new UniversityLibrarian();
	favoriteLibrarian.name = 'Ann';
	favoriteLibrarian.assistFaculty = () => console.log('test'); // works
	favoriteLibrarian.teachCommunity = () => console.log('test'); // will cause error

	favoriteLibrarian.assistFaculty();
	favoriteLibrarian.teachCommunity();
}

// #endregion

// #region Task 22 - Callback Functions

function Task22 () {
	console.log('Begin...');

	util.getBooksByCategory(Category.JavaScript, util.logCategorySearch);
	util.getBooksByCategory(Category.Software, util.logCategorySearch);

	console.log('End...');
}

// #endregion

// #region Task23 - Promises


function Task23() {

	console.log('Begin...');

	util.getBooksByCategoryPromise(Category.JavaScript)
		.then((titles) => {
			util.logBookTitles(titles);
			return titles.length;
		})
		.then((numOfTitles) => console.log(`Number of titles: ${numOfTitles}`))
		.catch((error) => console.log(error))
		;

	console.log('End...');
}

// #endregion

// #region Task24 - Async/Await

function Task24() {
	console.log('Beginning search...');

	util.logSearchResults(Category.JavaScript)
	.catch(reason => console.log(reason));

	console.log('Search submitted...');
}

// #endregion