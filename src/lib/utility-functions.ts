import { Category } from '../enums';
import { Book, LibMgrCallback } from '../interfaces';

export function showHello(divName: string, name: string) {
	const elt = document.getElementById(divName);
	elt.innerText = `Hello from ${name}`;
}

export function getAllBooks(): Book[] {
	const books: Book[] = [
		{
			id: 1,
			title: 'Refactoring JavaScript',
			author: 'Evan Burchard',
			available: true,
			category: Category.JavaScript
		},
		{
			id: 2,
			title: 'JavaScript Testing',
			author: 'Liang Yuxian Eugene',
			available: false,
			category: Category.JavaScript
		},
		{
			id: 3,
			title: 'CSS Secrets',
			author: 'Lea Verou',
			available: true,
			category: Category.CSS
		},
		{
			id: 4,
			title: 'Mastering JavaScript Object-Oriented Programming',
			author: 'Andrea Chiarelli',
			available: true,
			category: Category.JavaScript
		}
	];
	return books;
}

export function getBookByID(id: number): Book {
	return getAllBooks().find(
		(element) => {
			return element.id === id;
		});
}

export function getBookByCategory(category: Category = Category.JavaScript): Array<string> {
	const allBooks = getAllBooks();
	const titles: string[] = [];

	for (const book of allBooks) {
		if (book.category === category) {
			titles.push(book.title);
		}
	}

	return titles;
}

export function checkoutBooks(customer: string, ...bookIds: number[]) {
	console.log(`Customer name is '${customer}'`);

	let titles: string[] = [];

	for (const id of bookIds) {
		let book = getBookByID(id);

		if (book && book.available) {
			titles.push(book.title);
		}
	}

	return titles;
}

function getBookTitles(author: string): string[];
function getBookTitles(available: boolean): string[];
function getBookTitles(bookProperty: any): string[] {
	const allBooks = getAllBooks();

	if (typeof (bookProperty) === 'string') {
		return allBooks
			.filter(book => { return book.author === bookProperty; })
			.map(book => book.title);
	}
	else if (typeof (bookProperty) === 'boolean') {
		return allBooks
			.filter(book => { return book.available === bookProperty; })
			.map(book => book.title);
	}
}
export { getBookTitles };

export function printBook(book: Book): void {
	console.log(`${book.title} by ${book.author}`);
}

export function logFirstAvailable(books: any[] = getAllBooks()) {
	const numberOfBooks: number = books.length;
	let firstAvailableBookTitle: string;

	for (const book of books) {
		if (book.available) {
			firstAvailableBookTitle = book.title;
			break;
		}
	}
	console.log(`Total Books: ${numberOfBooks}`);
	console.log(`First Available book: ${firstAvailableBookTitle}`);
}

export function logBookTitles(titles: string[]): void {
	titles.forEach(element => {
		console.log(element);
	});
}

export function createCustomerId(name: string, id: number): string {
	return name + id;
}

export function createCustomer(name: string, age?: number, city?: string) {
	console.log(`Customer name is ${name}`);

	if (age)
		console.log(`Age is ${age}`);

	if (city)
		console.log(`City is ${city}`);
}

export function purge<T>(inventory: Array<T>): Array<T> {
	return inventory.slice(2);
}

export function getBooksByCategory(category: Category, callback: LibMgrCallback): void {
	setTimeout(() => {
		try {
			const titles = getBookByCategory(category);
			if (titles.length > 0) {
				callback(null, titles);
			}
			else throw new Error('No books found');
		}
		catch (error) {
			callback(error, null);
		}
	}, 2000);
}

export function getBooksByCategoryPromise(category: Category): Promise<Array<string>> {
	const p: Promise<string[]> = new Promise((resolve, reject) => {
		setTimeout(() => {
			const titles = getBookByCategory(category);
			if (titles.length > 0) {
				resolve(titles);
			}
			else reject('No books found');
		}, 2000);
	});

	return p;
}

export function logCategorySearch(error: Error, titles: string[]): void {
	if (error) {
		console.log(error.message);
	}
	else {
		logBookTitles(titles);
	}
}

export async function logSearchResults(category: Category) {
	let foundBooks = await getBooksByCategoryPromise(category);
	logBookTitles(foundBooks);
}