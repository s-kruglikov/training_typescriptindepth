import { ReferenceItem } from './classes';

export default class Encyclopedia extends ReferenceItem {

	constructor(title: string, year: number, public edition: number) {
		super(title, year);
	}

	printItem(): void {
		super.printItem();
		console.log(`Edition: ${this.edition}, Year: ${this.year}`);
	}

	printCitation(): void {
		console.log(`Title: ${this.title}, Year: ${this.year}`);
	}
}