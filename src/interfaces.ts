import { Category } from './enums';

interface Book {
	id: number;
	title: string;
	author: string;
	available: boolean;
	category: Category;
	year?: number;
	pages?: number;
	copies?: number;
	// markDamaged?: (reason: string) => void;
	markDamaged?: DamageLogger;
	[propName: string]: any;
}

interface DamageLogger {
	(input: string): void;
}

interface Person {
	name: string;
	email: string;
}

interface Author extends Person {
	numBooksPublished: number;
}

interface Librarian extends Person {
	department: string;
	assistCustomer: (custName: string) => void;
}

interface Magazine {
	title: string;
	publisher: string;
}

interface ShelfItem {
	title: string;
}

interface LibMgrCallback {
	(err: Error, titles: string[]): void;
}

export {
	Book,
	DamageLogger as Logger,
	Person,
	Author,
	Librarian,
	Magazine,
	ShelfItem,
	LibMgrCallback
};