export function sealed(p: string) {
	return function (target: Function): void {
		console.log(`Sealing the constructor ${p}`);
		Object.seal(target);
		Object.seal(target.prototype);
	};
}

export function logger<TFunction extends Function>(target: TFunction): TFunction {
	let newConstructor: Function = function() {
		console.log('Creating new instance');
		console.log(target);
	};

	newConstructor.prototype = Object.create(target.prototype);
	newConstructor.prototype.constructor = target;

	return <TFunction>newConstructor;
}

export function writeable(isWritable: boolean) {
	return function (target: Object, method: string, descriptor: PropertyDescriptor) {
		console.log(`Decorating method: ${method}`);
		descriptor.writable = isWritable;
	};
}