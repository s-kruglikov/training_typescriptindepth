namespace Utility {
	export namespace Fees {
		export function CalculateFee(daysLate: number): number {
			return daysLate * 0.25;
		}
	}

	export function MaxBooksAllowed(age: number) {
		return age < 12 ? 3 : 10;
	}

	function privateFunction(): void {
		console.log('private');
	}
}