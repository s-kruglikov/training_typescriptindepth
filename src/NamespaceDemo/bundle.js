var Utility;
(function (Utility) {
    var Fees;
    (function (Fees) {
        function CalculateFee(daysLate) {
            return daysLate * 0.25;
        }
        Fees.CalculateFee = CalculateFee;
    })(Fees = Utility.Fees || (Utility.Fees = {}));
    function MaxBooksAllowed(age) {
        return age < 12 ? 3 : 10;
    }
    Utility.MaxBooksAllowed = MaxBooksAllowed;
    function privateFunction() {
        console.log('private');
    }
})(Utility || (Utility = {}));
/// <reference path="utility-functions.ts" />
var util = Utility.Fees;
var fee = util.CalculateFee(10);
console.log(fee);
