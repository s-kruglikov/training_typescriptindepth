import * as Interfaces from './interfaces';
import { sealed, logger, writeable } from './decorators';

// @sealed('UniversityLibrarian')
// @logger
class UniversityLibrarian implements Interfaces.Librarian {
	name: string;
	email: string;
	department: string;

	assistCustomer(custName: string): void {
		console.log(`University librarian assists customer ${custName}`);
	}

	// @writeable(true)
	assistFaculty(): void {
		console.log('assisting faculty');
	}

	// @writeable(false)
	teachCommunity(): void {
		console.log('teaching community');
	}
}

abstract class ReferenceItem {
	// title: string;
	// year: number;

	// constructor(newTitle: string, newYear: number){
	// 	console.log('Creating new ReferenceItem');
	// 	this.title = newTitle;
	// 	this.year = newYear;
	// }

	private _publisher: string;
	static _department: string = 'New department';

	get publisher(): string {
		return this._publisher.toUpperCase();
	}

	set publisher(newPublisher: string) {
		this._publisher = newPublisher;
	}

	constructor(public title: string, protected year: number) {
		console.log('Creating new ReferenceItem');
	}

	printItem(): void {
		console.log(`Department: ${ReferenceItem._department}`);
		console.log(`${this.title}_${this.year}`);
	}

	abstract printCitation(): void;
}

export {
	UniversityLibrarian,
	ReferenceItem
};